/** 
 *
 */
package com.tangomc.om;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.dom4j.DocumentException;

import com.tangomc.om.dto.OMObject;
import com.tangomc.om.dto.OMType;

import junit.framework.TestCase;

/**
 * 
 */
public class MergeTest extends TestCase {
	
	public void testMakeTemp(){
		Merge m = new Merge();
		try{
			File f = m.makeTemp(new File("blah.zip"));
			assertNotNull(f);
			assertTrue(f.exists());
			f.delete();
		} catch(Exception e){
			fail(e.getMessage());
		}
	}
	
	public void testAddToMap(){
		Merge m = new Merge();
		Map<OMType, List<OMObject>> map = new HashMap<OMType, List<OMObject>>();
		OMType omType = OMType.BO;
		OMObject omObj = new OMObject();
		omObj.setType(OMType.BO.getTypeId());
		m.addToMap(map, omObj);
		assertTrue(map.get(omType).size()==1);
		
		m.addToMap(map, omObj);
		assertTrue(map.get(omType).size()==2);
	}
	
	public void testExtractObjects() throws FileNotFoundException, DocumentException{
		Merge m = new Merge();
		File f = new File("test/1.xml");
		if(!f.exists()){
			fail("can find the file ["+f.getAbsolutePath()+"]");
		}
		Map<OMType, List<OMObject>> map =  m.extractObjects(f);		
		assertNotNull(map);
		assertEquals(1, map.size());
		assertEquals(2, map.get(OMType.BO).size());
		
	}
	
	public void testProcess() throws Exception{
		Merge m = new Merge();
		m.process("test/sample/file2.zip", "test/sample/test_all_objects.zip", "success.zip");
		File f = new File("success.zip");
		assertNotNull(f);
		assertTrue(f.exists());
		f.delete();		
	}
	
	public void testProcessLargeFiles() throws Exception{
		Merge m = new Merge();
		m.process("test/sample/01.zip", "test/sample/02.zip", "success.zip");
		File f = new File("success.zip");
		assertNotNull(f);
		assertTrue(f.exists());
		File folder = m.unzip(f);
		int actual = folder.listFiles().length;
		assertEquals(9849, actual);
		FileUtils.cleanDirectory(folder);
		folder.delete();
		f.delete();			
	}
	
	public void testDirProcess() throws Exception{
		Merge m = new Merge();
		m.process("test/sample", "success.zip", null);
		File f = new File("success.zip");
		assertNotNull(f);
		assertTrue(f.exists());
		f.delete();				
	}
}
