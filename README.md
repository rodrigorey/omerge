omMerge is a tool to merge IBM TRIRIGA Object Migration Packages. 

It will remove any duplicates and merge existing objects into the new OM file specified. 

For any conflicts on the same objects, the newer file will win when specifying directory.

To override certain OM use the 2 file approach, where file1 will take priority. 

`Requirements`

* jdk 1.8
* apache ant 1.9 or >

`Build`

1. clone this repo
2. cd into the directory
3. type 'ant deploy' to generate the build arifact, merge.jar, under /dist folder. 

`Usage`

simple merge of 2 files, which creates the newfile.zip open command prompt and type the following:

* java -jar merge.jar [file1.zip] [file2.zip] [newfile.zip]

or 

specify directory of OM packages 

* java -jar merge.jar [directory] [newfile.zip]

When using this utility, be sure to download/clone the repository and run 'ant deploy' at the command prompt so that the latest version of merge.jar is created.
