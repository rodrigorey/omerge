/** 
 *
 */
package com.tangomc.om;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import com.tangomc.om.dto.OMObject;
import com.tangomc.om.dto.OMType;

/**
 * Utility to merge together multiple OM packages into a single OM package. 
 * It handles removing duplicates and takes precedence for the first of two files. 
 */
public class Merge {
	
	public static final String NAME_ALL_OBJECTS = "AllObjects.xml";
	
	/**
	 * 
	 * @param file1
	 * @param file2
	 * @param newFile
	 * @throws Exception
	 */
	protected void process(String file1, String file2, String newFile) throws Exception {
		log("Starting merge "+ new Date() +"...");
		
		File firstFile = new File(file1);
		if(firstFile.isDirectory()){
			newFile = file2;
			//get just the zip files in the dir
			File[] files = firstFile.listFiles(new FilenameFilter() {
			    public boolean accept(File dir, String name) {
			        return name.toLowerCase().endsWith(".zip");
			    }
			});
			reverseSortFiles(files);			
			
			for(int i=0;i<files.length;i++){
				//log(f.getName()+" ["+ new Date(f.lastModified()) +"]");
				
				//first time, new file doesn't exist yet.
				if(i==0 && files.length >= 2){
					doFileComps(files[0].getPath(), files[1].getPath());
					i++;
				} else {
					//need to compare it to the new file now
					doFileComps(newFile, files[i].getPath());
				}	
				log("zipping into new om["+newFile+"]...");
				zipUpMergeDir(newFile);
				cleanUp();
			}
		} else {		
			doFileComps(file1, file2);
			
			log("zipping into new om["+newFile+"]...");
			zipUpMergeDir(newFile);			
			cleanUp();
		}
		
		log("Merge complete.");
	}
		
	/**
	 * @throws IOException 
	 * 
	 */
	private void cleanUp() throws IOException {
		//clean up
		log("cleaning up temp data...");
		File f = new File("temp");
		FileUtils.cleanDirectory(f);
		f.delete();
	}

	/**
	 * sorts the files by last modified date in desc order, so the newer files are listed first
	 * @param files
	 */
	private void reverseSortFiles(File[] files) {
		Arrays.sort(files, new Comparator<File>(){
		    public int compare(File f1, File f2) {
		    	//switch it up to reverse sort... we want newest first
		        return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
		    } 
	    });
	}

	/**
	 * @param file1
	 * @param file2
	 * @throws Exception 
	 */
	protected void doFileComps(String file1, String file2) throws Exception {
		//make a new directory to put the exploded files.
		log("unzipping first om ["+file1+"]");
		File dirA = unzip(new File(file1));
		
		log("Unzipping second om ["+file2+"]");
		File dirB = unzip(new File(file2));
		
		//if we are still here, run the compare.
		log("Comparing files...");
		compare(dirA, dirB);
	}

	/**
	 * create the maps for both files and send to merge
	 * @param dirA
	 * @param dirB
	 * @throws DocumentException 
	 * @throws IOException 
	 */
	protected void compare(File dirA, File dirB) throws DocumentException, IOException{
		File fileA = new File(dirA, NAME_ALL_OBJECTS);
		File fileB = new File(dirB, NAME_ALL_OBJECTS);
		
		Map<OMType, List<OMObject>> mapA = extractObjects(fileA);
		Map<OMType, List<OMObject>> mapB = extractObjects(fileB);
		
		FilesDir fd1 = new FilesDir(dirA.getName(), mapA);
		FilesDir fd2 = new FilesDir(dirB.getName(), mapB);
		
		log("Merging files...");
		merge(fd1, fd2);
	}

	/**
	 * scan the different map objects and write out to disk a merged allobjects file. 
	 * @param fd1
	 * @param fd2
	 * @throws IOException 
	 */
	protected void merge(FilesDir fd1, FilesDir fd2) throws IOException{
		File dir = new File("merge");
		if(dir.exists()){
			//remove all exising files
			for(File f : dir.listFiles()){
				f.delete();
			}
		}
		
		Map<OMType, Collection<OMObject>> masterMap = new HashMap<OMType, Collection<OMObject>>();
		
		mergeMaps(fd1, fd2, masterMap);		
		mergeMaps(fd2, fd1, masterMap);
		
		writeToDisk(masterMap);
		
		compileFiles(fd1, fd2, masterMap);
	}
	
	/**
	 * @param fd1
	 * @param fd2
	 * @param masterMap
	 * @throws IOException 
	 */
	protected void compileFiles(FilesDir fd1, FilesDir fd2, Map<OMType, Collection<OMObject>> masterMap) throws IOException {		
		for(OMType omType : masterMap.keySet()){
			Collection<OMObject> omObjs = masterMap.get(omType);
			
			for(OMObject omObj : omObjs){
				String fileName = omObj.getFile();
				checkExistsAndCopy(fileName, fd1, fd2, omType);
			}			
		}		
	}

	/**
	 * checks the fileName in both lists and if only found in one, will copy from the temp 
	 * dir to the merge directory. fd1 will take precedence if found in both maps.
	 * @param fileName
	 * @param fd1
	 * @param fd2
	 * @param omType
	 * @throws IOException 
	 */
	protected void checkExistsAndCopy(String fileName, FilesDir fd1, FilesDir fd2, OMType omType) throws IOException {
		Map<OMType, List<OMObject>> mapA = fd1.getMap();
		Map<OMType, List<OMObject>> mapB = fd2.getMap();
		
		int locationCode = 0; // 1 is A, 2 is B, 3 is both
		Collection<OMObject> omA = mapA.get(omType);
		if(omA!=null){
			for(OMObject omo : omA){
				if(fileName.equals(omo.getFile())){
					locationCode=1;
					break;
				}
			}
		}
		
		Collection<OMObject> omB = mapB.get(omType);
		if(omB!=null){
			for(OMObject omo : omB){
				if(fileName.equals(omo.getFile())){
					locationCode+=2;
					break;
				}
			}
		}

		switch(locationCode){
		case 1:
			copyFile(fileName, fd1.getDirectoryName());
			break;
		case 2:
			copyFile(fileName, fd2.getDirectoryName());
			break;
		case 3:
			//it's in both packages, the first om package takes priority
			copyFile(fileName, fd1.getDirectoryName());
			break;
		}
	}

	/**
	 * @param fileName
	 * @param fd1
	 * @throws IOException 
	 */
	private void copyFile(String fileName, String dir) throws IOException {
		File source = new File("temp"+File.separator+dir+File.separator+fileName);
		FileUtils.copyFileToDirectory(source, new File("merge"));
	}

	/**
	 * Order matters for the maps passed in. only mapA is iterated.
	 * @param fd1
	 * @param fd2
	 * @param masterMap
	 */
	protected void mergeMaps(FilesDir fd1, FilesDir fd2, Map<OMType, Collection<OMObject>> masterMap) {
		Map<OMType, List<OMObject>> mapA = fd1.getMap();
		Map<OMType, List<OMObject>> mapB = fd2.getMap();
		
		for(OMType omType : mapA.keySet()){		
			log("processing ["+omType+"]...");
			
			List<OMObject> listA = mapA.get(omType);
			List<OMObject> listB = mapB.get(omType);
			
			//log("found ["+listA.size()+"] in file 1");
			
			Set<OMObject> masterSet = new HashSet<OMObject>();
			
			Set<OMObject> setA = new HashSet<OMObject>(listA);			
			if(listB!=null){
				//log("found ["+listB.size()+"] in file 2");
				
				//setA.removeAll(listB);
				Set<OMObject> setB = new HashSet<OMObject>(listB);			
				setB.removeAll(listA);
				//log("adding ["+setB.size()+"] from file2 to new");
				masterSet.addAll(setB);
			}	
			//log("adding ["+setA.size()+"] from file1 to new");
			masterSet.addAll(setA);			
			
			masterMap.put(omType, masterSet);
		}		
		
	}

	/**
	 * @param masterMap
	 * @throws IOException 
	 */
	private void writeToDisk(Map<OMType, Collection<OMObject>> masterMap) throws IOException {
		//should have a full list now just write it out or something.
		Document document = DocumentHelper.createDocument();
        Element root = document.addElement( "OMObjects" );
        for(OMType omType : masterMap.keySet()){
        	Set<OMObject> list = (Set<OMObject>)masterMap.get(omType);
        	for(OMObject omObj : list){
                omObj.toXML(root);
        	}        	
        }        
        OutputFormat format = OutputFormat.createCompactFormat();
        format.setEncoding("UTF-8");
        File f = new File("merge/AllObjects.xml");
        f.getParentFile().mkdirs();
        XMLWriter writer = new XMLWriter(new FileWriter(f), format);
        writer.write(document);
        writer.close();		
	}

	/**
	 * assumes the processing has been completed for the files and the <tt>merge</tt> 
	 * directory is just zipped up. 
	 * @param newFile
	 * @throws IOException 
	 */
	protected void zipUpMergeDir(String newFile) throws IOException {
		File mergeDir = new File("merge");
		zip(mergeDir.listFiles(), newFile);
		FileUtils.cleanDirectory(mergeDir);
		mergeDir.delete();
	}	
	
	/**
	 * put the AllObject.xml into an object that can then be used for comparison
	 * @param f
	 * @return
	 * @throws DocumentException 
	 * @throws FileNotFoundException 
	 */
	protected Map<OMType, List<OMObject>> extractObjects(File f) throws FileNotFoundException, DocumentException{
		Map<OMType, List<OMObject>> map = new HashMap<OMType, List<OMObject>>();
		SAXReader reader = new SAXReader();		
	    Document doc = reader.read(new FileInputStream(f));
		
	    List<?> list = doc.selectNodes("//OMObjects/OMObject");

        for (Iterator<?> iter = list.iterator(); iter.hasNext(); ) {
            Element element = (Element) iter.next();
            OMObject omObject = new OMObject(element);
            addToMap(map, omObject);
        }
	    log("found ["+map.size()+"] objects in ["+f.getAbsolutePath()+"]");
		return map;
	}
	
	protected void addToMap(Map<OMType, List<OMObject>> map, OMObject omObj){
		List<OMObject> list = null;
		OMType omType = omObj.getOMType();
		if(map.get(omType)==null){
			list = new ArrayList<OMObject>();
			list.add(omObj);
			map.put(omType, list);
			return;
		}
		list = map.get(omType);
		list.add(omObj);
	}
	
	protected File unzip(File zipFile) throws Exception{
		File dest = makeTemp(zipFile);
		
        FileInputStream fis;
        //buffer for read and write data to file
        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(zipFile);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry ze = zis.getNextEntry();
            while(ze != null){
                String fileName = ze.getName();
                File newFile = new File(dest + File.separator + fileName);
                //log("Unzipping to "+newFile.getAbsolutePath());
                //create directories for sub directories in zip
                new File(newFile.getParent()).mkdirs();
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                	fos.write(buffer, 0, len);
                }
                fos.close();
                //close this ZipEntry
                zis.closeEntry();
                ze = zis.getNextEntry();
            }
            //close last ZipEntry
            zis.closeEntry();
            zis.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return dest;
	}
	
	protected static File zip(File[] files, String newFile) throws IOException {
		File returnFile = new File(newFile);
		
		ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(returnFile));
		try {
			for(File file : files){
				ZipEntry entry = new ZipEntry(file.getName());
				zos.putNextEntry(entry);

				FileInputStream fis = null;
				try {
					fis = new FileInputStream(file);
					byte[] byteBuffer = new byte[4096];
					int bytesRead = -1;
					while ((bytesRead = fis.read(byteBuffer)) != -1) {
						zos.write(byteBuffer, 0, bytesRead);
					}
					zos.flush();
				} finally {
					IOUtils.closeQuietly(fis);
				}				
			}
			zos.closeEntry();
			zos.flush();
		} finally {
			IOUtils.closeQuietly(zos);
		}
		return returnFile;
	}	
	
	protected File makeTemp(File zipFile) throws Exception{
		String dest = "temp";
		new File(dest).mkdir();
		File destDir = new File(dest+File.separator+getFileName(zipFile));
		if(destDir.exists()){
			File[] files = destDir.listFiles();
			if(files!=null){
				for(File f : files){
					f.delete();
				}
			}
		} else if(!destDir.mkdirs()){
			throw new Exception(String.format("couldn't make dir for %s", destDir.getAbsolutePath()));
		}
		return destDir;
	}
	
	private String getFileName(File f){
		if(null==f){
			return null;
		}
		String name = f.getName();
		return name.substring(0, name.indexOf("."));
	}
	
	/**
	 * @param args
	 */
	private static void checkArgs(String[] args) {
		try{
			if(args==null || (args.length!=3 && args.length!=2)){
				throw new Exception();	
			} 	
		} catch(Exception e){
			printUsage();
			System.exit(1);
		}
	}

	/**
	 * 
	 */
	private static void printUsage() {
		String NEW_LINE = "\n";
		String TAB 		= "\t";
		
		StringBuilder sb = new StringBuilder();
		sb.append("Usage:").append(NEW_LINE)
		.append(TAB).append("merge [omFile1.zip] [omFile2.zip] [newOmfile.zip]").append(NEW_LINE);
		System.out.print(sb.toString());		
	}	
	
	/**
	 * main 
	 * @param args
	 */
	public static void main(String[] args){
		checkArgs(args);
		Merge m = new Merge();
		try{
			String arg3 = null;
			if(args.length==3){
				arg3 = args[2];
			}
			m.process(args[0], args[1], arg3);
		} catch(Exception e){
			System.err.print(e);
			System.exit(1);
		}
	}
	
	private void log(String s){
		System.out.println(s);
	}
	
	public static class FilesDir {
		private String directoryName;
		private Map<OMType, List<OMObject>> map;
		
		public FilesDir(String name, Map<OMType, List<OMObject>> map){
			this.directoryName = name;
			this.map = map;
		}
		/**
		 * @return the name
		 */
		public String getDirectoryName() {
			return directoryName;
		}
		/**
		 * @return the map
		 */
		public Map<OMType, List<OMObject>> getMap() {
			return map;
		}		
	}
	
}
