/** 
 *
 */
package com.tangomc.om.dto;

/**
 * 
 */
public enum OMType {	
	//this list is pulled from the OM web page drop down source for platform version 3.3.2
	BO("bo", 1),
	FORM("form", 2),
	WORKFLOW("workflow", 3),
	QUERY("query", 4),
	SCORECARD("scorecard", 5),
	MODULE("module", 6), 
	//no number 7
	LIST("list", 8),
	PORTAL("portal", 9),
	PORTAL_SECTION("portal_section", 10),
	//no 11
	DOCUMENT("doument", 12), 
	RECORD_DATA("record", 13),
	GROUP("group", 14),
	STYLES("styles", 15),
	//no 16
	BUDGET_TOKEN("budget_token", 17),
	//no 18-23
	IMAGE("images", 22),
	HIERARCHY_STRUCTURE("hierarchy_structure", 24),
	NAVIGATION_COLLECTION("navigation_collection", 25),
	NAVIGATION_ITEM("navigation_item", 26),
	ALTERNATE_FORM_LIST("alternate_form_list", 27),
	CALENDAR_SET("calendar_set", 28);
	
	private int typeId;
	private String type;
	
	OMType(String type, int typeId){
		this.typeId = typeId;
		this.type = type;
	}
	
	public static String getTypeStringFromId(int typeId){
		for(OMType omType : OMType.values()){
			if(typeId==omType.typeId){
				return omType.type;
			}
		}
		return null;
	}
	
	public static OMType getTypeFromId(int typeId){
		for(OMType omType : OMType.values()){
			if(typeId==omType.typeId){
				return omType;
			}
		}
		return null;
	}
	
	public String getType(){
		return this.type;
	}
	
	public int getTypeId(){
		return this.typeId;
	}
}
