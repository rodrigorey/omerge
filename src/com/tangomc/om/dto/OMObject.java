/** 
 *
 */
package com.tangomc.om.dto;

import org.dom4j.Element;

/**
 * Holds an entry for a object in the AllObjects.xml file that will reference another file.
 */
public class OMObject {

	public static final String NAME_MODULE = "Module";
	public static final String NAME_BO = "BO";
	public static final String NAME_NAME = "Name";
	public static final String NAME_FILE = "File";
	public static final String NAME_CONTENT_TYPE = "ContentType";
	public static final String NAME_CONTENT_FILE = "ContentFileName";
	public static final String ATTR_TYPE = "Type";
		
	private String module, bo, name, file, contentType, contentFileName;
	private int type;
	
	/**
	 * extract the object from the element passed in.
	 * @param element
	 */
	public OMObject(Element element){
		this.type = Integer.valueOf(element.attributeValue("Type"));
		this.module = element.elementText(NAME_MODULE);
		this.bo = element.elementText(NAME_BO);
		this.name = element.elementText(NAME_NAME);
		this.file = element.elementText(NAME_FILE);
		if(this.type==OMType.IMAGE.getTypeId()){
			this.contentType = element.elementText(NAME_CONTENT_TYPE);
			this.contentFileName = element.elementText(NAME_CONTENT_FILE);
		}
	}

	/**
	 * 
	 */
	public OMObject() {
	}

	/**
	 * @return the module
	 */
	public String getModule() {
		return module;
	}

	/**
	 * @param module the module to set
	 */
	public void setModule(String module) {
		this.module = module;
	}

	/**
	 * @return the bo
	 */
	public String getBo() {
		return bo;
	}

	/**
	 * @param bo the bo to set
	 */
	public void setBo(String bo) {
		this.bo = bo;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the file
	 */
	public String getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	public OMType getOMType(){
		return OMType.getTypeFromId(getType());
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the contentFileName
	 */
	public String getContentFileName() {
		return contentFileName;
	}

	/**
	 * @param contentFileName the contentFileName to set
	 */
	public void setContentFileName(String contentFileName) {
		this.contentFileName = contentFileName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bo == null) ? 0 : bo.hashCode());
		result = prime * result
				+ ((contentFileName == null) ? 0 : contentFileName.hashCode());
		result = prime * result
				+ ((contentType == null) ? 0 : contentType.hashCode());
		result = prime * result + ((file == null) ? 0 : file.hashCode());
		result = prime * result + ((module == null) ? 0 : module.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + type;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OMObject other = (OMObject) obj;
		if (bo == null) {
			if (other.bo != null)
				return false;
		} else if (!bo.equals(other.bo))
			return false;
		if (contentFileName == null) {
			if (other.contentFileName != null)
				return false;
		} else if (!contentFileName.equals(other.contentFileName))
			return false;
		if (contentType == null) {
			if (other.contentType != null)
				return false;
		} else if (!contentType.equals(other.contentType))
			return false;
		if (file == null) {
			if (other.file != null)
				return false;
		} else if (!file.equals(other.file))
			return false;
		if (module == null) {
			if (other.module != null)
				return false;
		} else if (!module.equals(other.module))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	/**
	 * @return
	 */
	public Element toXML(Element root) {
		Element om = root.addElement("OMObject");
		om.addAttribute(OMObject.ATTR_TYPE, String.valueOf(getType()));                
        
		om.addElement(OMObject.NAME_MODULE).addCDATA(getModule());
        om.addElement(OMObject.NAME_BO).addCDATA(getBo());
        om.addElement(OMObject.NAME_NAME).addCDATA(getName());
        om.addElement(OMObject.NAME_FILE).addCDATA(getFile());    
        if(getType()==OMType.IMAGE.getTypeId()){
        	om.addElement(OMObject.NAME_CONTENT_TYPE).addCDATA(getContentType());
        	om.addElement(OMObject.NAME_CONTENT_FILE).addCDATA(getContentFileName());
        }
        return om;
	}
}
